<div align="center">
 <p align="center">
    Welcome to:
  </p>

  <h1 align="center">Digital Humanities</h3>

  <p align="center">
    Misha Velthuis & Daniël Kooij
  </p>

  <p align="center">
    Click on the image below in order to open the Binder environment.
  </p>


  [<img src="images/Digital_Humanities_button.png" alt="Button" width="500" height="300">](https://mybinder.org/v2/gl/ahimsa%2Fdigital_humanities_fps/HEAD)
  </a>

   <p align="center">
    If the image doesn't work, click [here](https://mybinder.org/v2/gl/ahimsa%2Fdigital_humanities_fps/HEAD).
  </p>


</div>


