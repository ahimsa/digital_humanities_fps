{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Twitter API Setup"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To collect Twitter data, we're going to work with the [Twitter API](https://developer.twitter.com/en/docs/basics/getting-started) and [twarc2](https://twarc-project.readthedocs.io/en/latest/twarc2/), a Python package for collecting Twitter data through the Twitter API.\n",
    "\n",
    "To use the Twitter API and twarc, we first need to complete the following steps:\n",
    "\n",
    "1. Apply for a Twitter developer account, which also requires making or already having a regular Twitter account\n",
    "\n",
    "2. Get Twitter API keys\n",
    "\n",
    "3. Install & configure twarc2\n",
    "\n",
    "The following instructions will help guide you through this process."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Apply for a Twitter Developer Account"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Twitter allows you to use the API for free through a twitter account with added permissions, called a developer account. If you are an undergraduate student or not affiliated with an academic institution, you can apply for [a standard developer account](https://developer.twitter.com/en/apply-for-access), which enables access to recent tweets as well as tweets in real time. Apply for this standard developer account with the link above, this will probably take a few minutes. Make sure you have access to the email you register with as you will have to enter a confirmation code once everything is set up. \n",
    "\n",
    "Good to know: If you are an academic researcher (e.g., \"post-doc, professor, fellow, student working on a thesis, PhD candidate, or affiliated with an academic institution AND have a clearly defined project\"), you can apply for [the Academic Research track]( https://developer.twitter.com/en/portal/petition/academic/is-it-right-for-you) of the Twitter API, which enables access to all Twitter data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Get API Keys and Bearer Token"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once you've applied for a developer account and it has been approved by Twitter (check your email), you have to go to the webpage https://developer.twitter.com/ to generate your API keys and bearer token. This is your unique key (or kind of like a password) to link your script (requests to the API) to your Twitter account. \n",
    "\n",
    "Make sure you record these keys - which are strings of random letters and numbers - on a secure and easy-to-find location on your computer (do not share them on the internet because these are private like paswords). \n",
    "\n",
    "If this webpage doesn't pop up and you cannot find the API-keys, you can follow [these instructions](https://github.com/twitterdev/getting-started-with-the-twitter-api-v2-for-academic-research/blob/main/modules/4-getting-your-keys-and-token.md) from Suhem Parack, the Developer Advocate for Academic Research at Twitter."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Install & configure twarc2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To install & configure twarc2, we will need to make use of the so-called terminal of this binder environment. As you maybe can remember, this Binder environment is hosted on a server; a computer \"in the cloud\", dedicated to access services on the internet (such as Binder, and any other website for that matter). \n",
    "\n",
    "In order to use the twitter API we need to install the twarc package in this Binder environment, thus on the server that hosts your current jupyter notebook session on Binder (see figure below). It is possible to install the twarc package through the terminal, which is the interface that executes commands on a computer.\n",
    "\n",
    "<div align=\"center\">\n",
    "  <img src=\"../../images/BinderServerEnvironment.png\" alt=\"Server\" width=\"800\" height=\"500\">\n",
    "  <br />\n",
    "  <em>Fig 1. Graphic overview of your computer, the terminal and the Binder server.</em>\n",
    "</div>\n",
    "\n",
    "<br />"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To open the terminal of this binder environment, look at the tabs at the top of this screen. One of the tabs is called \"Launcher\", click on this and afterwards click on the icon called \"terminal\". The images below visualize these steps. \n",
    "\n",
    "<div align=\"center\">\n",
    "  <img src=\"../../images/FindLauncher.PNG\" alt=\"Logo\" width=\"700\" height=\"500\">\n",
    "  <br />\n",
    "  <em>Fig 2. Location of the launcher tab</em>\n",
    "</div>\n",
    "\n",
    "<br />\n",
    "\n",
    "<div align=\"center\">\n",
    "  <img src=\"../../images/FindTerminal.PNG\" alt=\"Logo\" width=\"500\" height=\"500\">\n",
    "  <br />\n",
    "  <em>Fig 3. Location of the terminal icon</em>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When you opened the terminal you will see one green string of text. We are now able to directly give commands to the computer that this binder environment is hosted on. \n",
    "\n",
    "After the colon and dollar sign of this green string (:~$), type the following command line in the terminal and press enter:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "command_line"
    ]
   },
   "outputs": [],
   "source": [
    "pip install twarc --upgrade"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once twarc is installed, you need to configure it with your API keys and/or bearer token so that you can actually access the API."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To configure twarc, you can run the following from the command line:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "command_line"
    ]
   },
   "outputs": [],
   "source": [
    "twarc2 configure"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Twarc will ask for your bearer token, which you have saved earlier in step 2. Copy and paste the bearer token into the blank after the colon, and then press enter. This will connect twerc with the twitter API using your developer account. You can press _n_ and enter when the terminal is asking for API keys."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you've entered your information correctly, you should get a congratulatory message that looks something like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "command_line"
    ]
   },
   "outputs": [],
   "source": [
    "Your keys have been written to /home/.../.../twarc/config\n",
    "\n",
    "\n",
    "✨ ✨ ✨  Happy twarcing! ✨ ✨ ✨"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you're ready to collect and analyze tweets!"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
